export function setItem(key,value) {   
    console.log(key,value)
    if(!window.localStorage){
        return false;
    }else{
        var storage=window.localStorage;
		if(value){
			if(typeof value =='object'){
				storage.setItem(key,JSON.stringify(value));
			}else{
				storage.setItem(key,value);
			}
		}       
    }
}
/**
 * localStorage的读取
 */
export function getItem(key) {   
    var storage = window.localStorage;
    if(!storage){
        return false;
    }else{
        storage = storage.getItem(key);
        if(storage == "" || storage == undefined || storage == null){
            return false;
        }else{
            if(storage.indexOf("{") > -1){
                storage = JSON.parse(storage);
            }
        }
    }
   
    return storage;
}
/**
 * localStorage删除指定键对应的值
 */
export function removeItem(key) { 
    localStorage.removeItem(key);
}

/**
 * 判断是否为空
 */
function validatenull(val) {
    if (val instanceof Array) {
        if (val.length == 0) return true;
    } else if (val instanceof Object) {
        if (JSON.stringify(val) === '{}') return true;
    } else {
        if (val == 'null' || val == null || val == 'undefined' || val == undefined || val == '') return true;
        return false;
    }
    return false;

};

//获取cookie、
export function getCookie(name) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg))
        return (arr[2]);
    else
        return null;
}

//设置cookie,增加到vue实例方便全局调用
export function setCookie(value,c_name,expiredays) {
    if(!c_name) c_name='campus_token';
    if(!expiredays) expiredays=1000 * 60 * 60;
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    document.cookie = c_name + "=" + escape(value) + ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString());
};

//删除cookie
export function delCookie(name) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval = getCookie(name);
    if (cval != null)
        document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
};


export default {
    setItem,getItem,removeItem,getCookie,setCookie,delCookie
}