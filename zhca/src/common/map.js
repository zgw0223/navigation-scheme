//百度定位
//<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=A0riMy7Pv5rUmdgqwNF1Cmncnzg2UdGf"></script>		页面引入
var longitude, latitude,map = new BMap.Map("");
function my_position(callback){		//定位
	if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position){
			let point={
				longitude:position.coords.longitude,
				latitude:position.coords.latitude
			};
		//	alert(JSON.stringify(point));
			callback && callback(point)
		}, //成功回调函数
			function(error) { //失败回调函数
				showError(error);
			}, {
				enableHighAcuracy: true,
				timeout: 5000,
				maximumAge: 0
			}); // 这里设置超时为5000毫秒，即1秒
	}
}
function showError(error) {
	switch(error.code) {
		case error.PERMISSION_DENIED:
			alert("定位失败,用户拒绝请求地理定位");
			break;
		case error.POSITION_UNAVAILABLE:
			alert("定位失败,位置信息是不可用");
			break;
		case error.TIMEOUT:
			alert("定位失败,请求获取用户位置超时");
			break;
		case error.UNKNOWN_ERROR:
			alert("定位失败,定位系统失效");
			break;
	}
};

var localSearch = new BMap.LocalSearch(map);
localSearch.enableAutoViewport(); //允许自动调节窗体大小


function searchByStationName(keyword) {		//地址获取经纬度
　　let point;
　　localSearch.setSearchCompleteCallback(function (searchResult) {
　　　　var poi = searchResult.getPoi(0);
		point={
			lng:poi.point.lng,
			lat:poi.point.lat
		};
　　});
　　localSearch.search(keyword);
	return point;
}
export{
	my_position,searchByStationName
}