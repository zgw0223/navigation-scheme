import Vue from 'vue'
import App from './App.vue'
import Vant from 'vant';
import router from './router';
import api from '@/api/index'
import store from '@/store'
import http from '@/common/request'
import '@/common/vue'
import 'vant/lib/index.css';
import '@/assets/css/common.css';
import TdFooter from '@/common/custom/footer'
import TdUpload from '@/common/custom/upload'
import TdTimer from '@/common/custom/timer'
import VueCookies from 'vue-cookies'
import 'babel-polyfill'

import clipper from '@/common/clipper'
// Vue.use(VueCookies)
Vue.use(Vant).use(clipper);


Vue.prototype.$cookie =VueCookies
Vue.component('mt-footer', TdFooter)      //注册footer
Vue.component('td-upload', TdUpload)    //注册upload
Vue.component('td-timer', TdTimer)  //注册timer
Vue.config.productionTip = false
Vue.prototype.$api = api //调用api数据接口地址
//将 axios 改写为 Vue 的原型属性
Vue.prototype.$post = http.post
Vue.prototype.$get = http.get
Vue.prototype.$files = http.files

// document.addEventListener("deviceready", function(){
  new Vue({
    router,
    store,
    render: h => h(App),
  }).$mount('#app')
// }, false)