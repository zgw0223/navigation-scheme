
//高德地图定位
//<script type="text/javascript" src="https://webapi.amap.com/maps?v=1.4.11&key=839011ab77b2f87ec4261360b18b9f99"></script>
function my_position(callback) { //定位
	var map = new AMap.Map('');
	map.plugin('AMap.Geolocation', function() {
	  var geolocation = new AMap.Geolocation({
	    // 是否使用高精度定位，默认：true
	    enableHighAccuracy: true,
	    // 设置定位超时时间，默认：无穷大
	    timeout: 10000,
	    // 定位按钮的停靠位置的偏移量，默认：Pixel(10, 20)
	    buttonOffset: new AMap.Pixel(10, 20),
	    //  定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
	    zoomToAccuracy: true,     
	    //  定位按钮的排放位置,  RB表示右下
	    buttonPosition: 'RB'
	  })
	
	  geolocation.getCurrentPosition()
	  AMap.event.addListener(geolocation, 'complete', onComplete)
	  AMap.event.addListener(geolocation, 'error', onError)
	
	  function onComplete (data) {
	  	console.log(data);
	  	var data=data.addressComponent;
	  	callback && callback(data.province+data.city+data.district);
	  }
	
	  function onError (data) {
	    // 定位出错
	  }
	})

	
}
export {
	my_position
}