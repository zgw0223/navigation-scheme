const basePath = process.env.NODE_ENV == 'development' ? '/api/' : 'http://106.14.45.98:8086/';
//  const basePath = "http://106.14.45.98:8086/";

export default {
    //用户基础信息
    user:{
        login:basePath+'user/login',    //用户登录  POST
        logout:basePath+'user/logout',    //用户注销    POST
        info:basePath+'user/info',    //个人信息    GET
        update:basePath+'user/info/update',    //更改个人信息    POST
        posts:basePath+'user/posts',    //我发布的帖子    GET
        products:basePath+'user/products',    //我发布的商品    GET
        orders:basePath+'user/orders',    //我的订单    GET
        orderDetail:basePath+'user/orders/detail',    //我的订单    GET
        
        comments:basePath+'user/comments',    //我的评论    GET
        notices:basePath+'user/notices',    //我的消息    GET
        collectionsCommodities:basePath+'user/collections/commodities',    //我收藏的商品    GET
        collectionsUsers:basePath+'user/collections/users',    //我关注的用户    GET
        collectionsFollowers:basePath+'user/collections/followers',    //我的粉丝    GET
        follow:basePath+'user/account/follow',    //关注/取关用户    POST
        otherInfo:basePath+'user/other/info',    //他人信息    GET
        commodities:basePath+'user/commodities',    //我发布的商品    GET
        collect:basePath+'user/collect',    //取消/收藏(帖子/商品) POST
        like:basePath+'user/like',    //点赞帖子/商品/评论  POST
        notices:basePath+'user/notices',    //我的消息  GET
        suggestion:basePath+'user/suggestion',    //意见反馈  POST
        report:basePath+'user/report',    //举报  POST
        appeal:basePath+'user/appeal',    //申诉  POST
        
        postsThumb:basePath+'user/posts/thumb',  //我点赞的帖子  //  GET
        commoditiesThumb:basePath+'user/commodities/thumb',    //我点赞的商品  POST

        refresh:basePath+'user/token/refresh',  //用户令牌刷新  //  POST
    },
    blog:{
        accounts:basePath+'blog/accounts',    //推荐公众号列表 GET
        posts:basePath+'blog/posts',    //帖子/失物招领列表 GET
        postsDetail:basePath+'blog/posts/detail',    //帖子详情 GET
        postsPublish:basePath+'blog/posts/publish',    //发布帖子/失物招领 POST
        postsComments:basePath+'blog/posts/comments',    //更多评论 GET
        postsDelete:basePath+'blog/post/delete',    //帖子删除 POST
        lostsPublish:basePath+'blog/losts/publish',    //发布失物招领 POST
        commentsPublish:basePath+'blog/comments/publish',    //发布评论 POST
        lostFinish:basePath+'blog/lost/finish',    // 失物招领完成 POST
        
    },
    market:{
        category:basePath+'market/category',    //分类列表 GET
        commodities:basePath+'market/commodities',    //商品列表 GET
        detail:basePath+'market/commodities/detail',    //商品详情 GET
        productsPublish:basePath+'market/commodities/publish',    //发布商品 POST
        buy:basePath+'market/commodities/buy',    //开始/取消交易流程 POST
        delete:basePath+'market/commodity/delete',    //删除商品 POST
        finish:basePath+'market/order/finish',    //交易完成 POST
    },
    sms:basePath+'sms',    //获取验证码  GET
    searchPosts:basePath+'search/posts',    //帖子搜索/失物招领搜索  GET
    searchCommodities:basePath+'search/commodities',    //商品搜索  GET
    upload:basePath+'photo/upload',    //上传图片  POST
    photoUrl:'http://106.14.45.98/',    //图片地址
}
