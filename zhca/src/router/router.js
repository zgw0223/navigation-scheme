const _import = require('@/router/_import')

// 作为Main组件的子页面展示并且在左侧菜单显示的路由写在appRouter里
export const appRouter = [
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/login',
        name : 'login',
        component: _import('login'),
    },
    {
        path: '/code_login',
        name : 'code_login',
        component: _import('code_login'),
    },
    {
        path: '/home',
        name : '首页',
        component: _import('home/index'),
    },
    {
        path: '/type',
        name : '分类',
        component: _import('type/index'),
    },
    {
        path: '/shopCart',
        name : '购物车',
        component: _import('shopCart/index'),
    },
    
    {
        path: '/orderDetail2',
        name : '订单详情(已完成)',
        component: _import('order/orderDetail2'),
    },
    {
        path: '/address',
        name : '我的地址',
        component: _import('person/address'),
    },


    {
        path: '/person',
        name : '个人中心',
        component: _import('person/index'),
    },
    {
        path: '/otherUser',
        name : '用户信息',
        component: _import('person/otherUser'),
    },
    {
        path: '/person_info',
        name : '资料编辑',
        component: _import('person/person_info'),
    },
    {
        path: '/shop',
        name : '商品列表',
        component: _import('shop/index'),
    },
    {
        path: '/shopDetail',
        name : '商品详情',
        component: _import('shop/shopDetail'),
    },
    {
        path: '/releaseShop',
        name : '发布商品',
        component: _import('shop/releaseShop'),
    },
    {
        path: '/releaseysShop',
        name : '发布失物招领',
        component: _import('shop/releaseysShop'),
    },
    {
        path: '/releaseInvitation',
        name : '发布帖子',
        component: _import('shop/releaseInvitation'),
    },
    {
        path: '/search',
        name : '搜索',
        component: _import('search'),
    },
    {
        path: '/invitation',
        name : '我的帖子',
        component: _import('person/invitation'),
    },
    {
        path: '/invitationContent',
        name : '帖子详情',
        component: _import('person/invitationContent'),
    },
    {
        path: '/collect',
        name : '我的收藏',
        component: _import('person/collect'),
    },
    {
        path: '/myShop',
        name : '我的商品',
        component: _import('person/myShop'),
    },
    {
        path: '/myShopDetail',
        name : '我的商品详情',
        component: _import('person/myShopDetail'),
    },
    {
        path: '/shopFinish',
        name : '我的商品详情（已完成）',
        component: _import('person/shopFinish'),
    },
    {
        path: '/setting',
        name : '系统设置',
        component: _import('set/setting'),
    },
    {
        path: '/aboutUs',
        name : '关于我们',
        component: _import('set/aboutUs'),
    },
    {
        path: '/suggestion',
        name : '意见反馈',
        component: _import('set/suggestion'),
    },
    {
        path: '/appeal',
        name : '申诉',
        component: _import('set/appeal'),
    },
    {
        path: '/report',
        name : '举报',
        component: _import('set/report'),
    },
    {
        path: '/comment',
        name : '评论',
        component: _import('set/comment'),
    },
    {
        path: '/leaveMessage',
        name : '留言',
        component: _import('set/leaveMessage'),
    },
    /* 2019-07-13 oxw */
    {
        path: '/concern',
        name : '我的关注',
        component: _import('person/concern'),
    },
    {
        path: '/fans',
        name : '我的粉丝',
        component: _import('person/fans'),
    },
    {
        path: '/gzh',
        name : '推荐公众号',
        component: _import('person/gzh'),
    },
    {
        path: '/comments',
        name : '我的评论',
        component: _import('person/comments'),
    },
    {
        path: '/message',
        name : '消息',
        component: _import('person/message'),
    },
    {
        path: '/message1',
        name : '系统通知',
        component: _import('person/message/message1'),
    },
    {
        path: '/message2',
        name : '评论',
        component: _import('person/message/message2'),
    },
    {
        path: '/order',
        name : '订单列表',
        component: _import('order/index'),
    },
    {
        path: '/orderDetail1',
        name : '订单详情',
        component: _import('order/orderDetail1'),
    },

    {
        path: '/map',
        name : '服务',
        component: _import('map/index'),
    },
    {
        path: '/mapDetail',
        name : '详情',
        component: _import('map/detail'),
    },


    

    

];

// 所有上面定义的路由都要写在下面的routers里
export const routers = [
    // loginRouter,
    // ...otherRouter,
    ...appRouter
];
